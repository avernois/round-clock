local modname = ...
local M = {}
_G[modname] = M

function M.sync_ntp(success)
    net.dns.resolve("0.fr.pool.ntp.org", 
        function(sk, ip)
            restart = function () M.sync_ntp(success) end
            if (ip == nil) 
            then 
                print("DNS fail!, will retry in 1s") 
                timer = tmr.create()
                timer.alarm(timer, 1000, tmr.ALARM_SINGLE, restart)
            else 
                print(ip) 
                sntp.sync(ip, 
                    function(sec,usec,server)
                        print('sync', sec, usec, server)
                        success()
                    end,
                    function()
                        print("sync failed, will retry in 1s")
                        timer = tmr.create()
                        timer.alarm(timer, 1000, tmr.ALARM_SINGLE, restart)
                    end)
            end
        end)
end

local function timer_callback()
    M.sync_ntp(function() end)
end

local timer = tmr.create()
timer:register(60*60*1000, tmr.ALARM_AUTO, timer_callback)


function M.autosync(delay_ms)
    if delay_ms ~= nil then
        timer:interval(delay_ms)
    end
    timer:start()
end

function M.stop_autosync()
    timer:stop()
end



function M.get_zoned_time(time_shift)
    timestamp = rtctime.get()
    return rtctime.epoch2cal(timestamp + time_shift * 60 * 60)
end


return M
