
print("heap", node.heap())
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, function(T)
    print("\n\tSTA - GOT IP".."\n\tStation IP: "..T.IP.."\n\tSubnet mask: "..T.netmask.."\n\tGateway IP: "..T.gateway)
    wifi.setmode(wifi.STATION, true)
    start()
end)

local config = {
    hour =     {red=   0, green = 255, blue =   0},
    minute =   {red= 128, green = 128, blue =   0},
    second =   {red= 100, green = 100, blue = 100},
    hour_dot = {red=   0, green =   0, blue =  64}
}


local display = require "display"
display.init(config, 44)
print("display", node.heap())

local time = require "time"
print("time", node.heap())
local clock = require "linear_clock"
clock.init(time, display, config)
print("clock", node.heap())

local server = require "config_server"
server.init(config)
server.start()
print("config_server", node.heap())

local ap_cfg =
{
    ip="192.168.4.1",
    netmask="255.255.255.0",
    gateway="192.168.4.1",
    ssid="Linear-"..string.format('%02x', node.chipid()),
    auth=wifi.OPEN
}

wifi.eventmon.register(wifi.eventmon.WIFI_MODE_CHANGED, 
    function(T)
        print("\n\tSTA - WIFI MODE CHANGED".."\n\told_mode: ".. T.old_mode.."\n\tnew_mode: "..T.new_mode)
        if (T.new_mode == wifi.STATIONAP or T.new_mode == wifi.SOFTAP )then
            print("Configuring AP")
            res=wifi.ap.config(ap_cfg)
            
            print("config ap: ", res)
            res=wifi.ap.setip(ap_cfg)
            print("set ip: ", res)
        
            print("\n  Current SoftAP configuration:")
            for k,v in pairs(wifi.ap.getconfig(true)) do
                print("   "..k.." :",v)
            end
        end
    end
)

function ap_start()
    print("Starting Access Point?")
    if (wifi.sta.getip() == nil) then
        wifi.setmode(wifi.STATIONAP, false)
        dns = require "fake_dns"
        dns.start()
    else
        print("nope, already got an ip")
    end
end

local ap_timer = tmr.create()
ap_timer:alarm(5000, tmr.ALARM_SINGLE, ap_start)

function start()
    time.sync_ntp(function() clock.start() end)
end

package.loaded.linear_clock = nil
package.loaded.display = nil
package.loaded.config_server = nil
