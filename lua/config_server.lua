local M = {}

local print = print
local stringFormat = string.format
local stringMatch = string.match
local tonumber = tonumber
local package = package
local require = require
local wifiStaConfig = wifi.sta.config

if setfenv then
    setfenv(1, M) -- for 5.1
else
    _ENV = M -- for 5.2
end

local config

local function toHexColor(color)
    return stringFormat('#%02x%02x%02x', color.red, color.green, color.blue)
end

local function configPage()
   return 'HTTP/1.1 200 OK\n\n'..
   '<html>'..
   '<title>Linear Clock</title>'..
   '<body><h1>Configuration</h1>'..
   '<br>'..
   '<form action="/" method="POST">'..

   'Hour color: <input type="color" name="hour" value="'.. toHexColor(config.hour) .. '"><br>'..
   'Minute color: <input type="color" name="minute" value="'.. toHexColor(config.minute) .. '"><br>'..
   'Second color: <input type="color" name="second" value="'.. toHexColor(config.second) .. '"><br>'..

   'Hour dot color: <input type="color" name="hour_dot" value="'.. toHexColor(config.hour_dot) .. '"><br>'..
   
   '<br>'..
   '<input type="submit">'..
   '</form>'..
   '<br>'..
   '<form action="/wifi" method="POST">'..

   'SSID: <input type="text" name="wifi_ssid" value=""><br>'..
   'Password: <input type="password" name="wifi_password" value=""><br>'..   
   '<br>'..
   '<input type="submit">'..
   '</form>'..
   '</body></html>'
end

local function extractColor(name, payload)
    local red=stringMatch(payload, name.."=%%23(%x%x)")
    local green=stringMatch(payload, name.."=%%23%x%x(%x%x)%x%x")
    local blue=stringMatch(payload, name.."=%%23%x%x%x%x(%x%x)")
    red = tonumber(red, 16)
    green = tonumber(green, 16)
    blue = tonumber(blue, 16)

    return {red = red, green = green, blue = blue}
end

local function setColor(request)
    config.hour = extractColor("hour", request.body)
    config.minute = extractColor("minute", request.body)
    config.second = extractColor("second", request.body)
    config.hour_dot = extractColor("hour_dot", request.body)
end

local function extractCredentials(request)
    local ssid = stringMatch(request.body, "wifi_ssid=(.*)&")
    local password = stringMatch(request.body, "wifi_password=(.*)")
    
    return ssid, password
end

local function configureWifi(ssid, password)
    local sta_cfg = {
        ssid=ssid,
        pwd=password,
        auto=true,
        save=true
    }
    wifiStaConfig(sta_cfg)
    return configPage()
end

function M.init(configuration)
    config = configuration
end

function M.start()
    server = require "server"
    server.addRoute("GET", "/generate_204", function() end, function() return 'HTTP/1.1 204 OK\n\n' end)
    server.addRoute("GET", "/", function() end, configPage)
    server.addRoute("POST", "/", setColor, configPage)
    server.addRoute("POST", "/wifi", extractCredentials, configureWifi)
    server.addRoute("GET", "/wifi", function() end, configPage)
    server.start()
    server=nil
    package.loaded.server = nil
end


return M
