local fake_dns = {}

local tableInsert = table.insert
local netCreateUDPSocket, net_TCP = net.createUDPSocket, net.TCP
local print, ipairs, tonumber = print, ipairs, tonumber
local stringFind, stringMatch, stringGsub, stringChar, stringSub = string.find, string.match, string.gsub, string.char, string.sub
local setmetatable = setmetatable

if setfenv then
    setfenv(1, fake_dns) -- for 5.1
else
    _ENV = fake_dns -- for 5.2
end



local function unhex(str)
    str = stringGsub (str, "(%x%x) ?",
        function(h) return stringChar(tonumber(h,16)) end)
    return str
end

local dns_server 


function fake_dns.start()
    dns_server= netCreateUDPSocket()
    dns_server:listen(53)
    dns_server:on("receive", function(socket, data, request_port, request_ip)
        print("Got a request from ", request_port, request_ip)
        local transaction_id=stringSub(data,1,2)
        local flags=stringSub(data,3,4)
        local questions=stringSub(data,5,6)
    
        query = ""
        raw_query = ""
        j=13
        while true do
            byte = stringSub(data,j,j)
            j=j+1
            raw_query = raw_query .. byte
            if byte:byte(1)==0x00 then --NULL marks end of the string.
                break
            end
            for i=1,byte:byte(1) do
                byte = stringSub(data,j,j)
                j=j+1
                raw_query = raw_query .. byte
                query = query .. byte
            end
            query = query .. '.'
        end
        query=query:sub(1,query:len()-1) --strip the trailing dot.
        q_type = stringSub(data,j,j+1)
        j=j+2
        if q_type == unhex("00 01") then 
            --print("Got a type A query "..query)
            class = stringSub(data,j,j+1)
        
            --ip = "192.168.4.1"
            ip=unhex("C0 A8 04 01")
            answers = unhex("00 01")
            flags = unhex("81 80")
        
            resp=transaction_id..flags..questions..answers..unhex("00 00")..unhex("00 00")..raw_query..q_type..class
            resp=resp..unhex("c0 0c")..q_type..class..unhex("00 00 00 da")..unhex("00 04")..ip
            socket:send(request_port, request_ip, resp)
        end
    end)
    
    dns_server:on("sent",function(s) 
    end)
end


function fake_dns.stop()
    dns_server:close()
end


return fake_dns