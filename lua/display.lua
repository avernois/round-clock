local display = {}
local print = print
local ws2812 = ws2812
if setfenv then
    setfenv(1, display) -- for 5.1
else
    _ENV = display -- for 5.2
end

local offset

local default_config = {
    hour     = {red=   0, green = 255, blue =   0},
    minute   = {red= 255, green =   0, blue =   0},
    second   = {red= 100, green = 100, blue = 100},
    hour_dot = {red=   0, green =   0, blue =  64}
}

local buffer = ws2812.newBuffer(60, 3)
local hour_b = ws2812.newBuffer(60, 3)
local min_b = ws2812.newBuffer(60, 3)
local sec_b = ws2812.newBuffer(60, 3)

local function limit_power(buffer)
    local psu_current_ma = 2000
    local led_current_ma = 20
    local led_sum = psu_current_ma * 255 / led_current_ma
    local power = buffer:power()

    if (power > led_sum) then
        print("limiting power", power, led_sum)
        buffer:buffer(256 * led_sum / power, buffer) -- power is now limited
    end
end

local function color(color)
    return color.green, color.red, color.blue
end

local function initWithHourDots(buf, dot_color)
    buf:fill(0, 0, 0)
    for i=0, 11 do
        
        buf:set((i*5 + offset)%60 + 1, color(dot_color))
    end
end


function display.init(config, led_offset)
    local colors = config or default_config
    offset = led_offset or 0
    ws2812.init()
    buffer:fill(0, 0, 0)
    ws2812.write(buffer)
end

function display.display(hour, min, sec, config)
    local colors = config or default_config
    initWithHourDots(buffer, colors.hour_dot)
    

    buffer:set(((hour%12)*5 - 1 + offset)%60 + 1, color(colors.hour))
    buffer:set(((hour%12)*5     + offset)%60 + 1, color(colors.hour))
    buffer:set(((hour%12)*5 + 1 + offset)%60 + 1, color(colors.hour))
    
    buffer:set((min + offset)%60 + 1, color(colors.minute))
    
    buffer:set((sec + offset)%60 + 1, color(colors.second))  

    limit_power(buffer)
    
    ws2812.write(buffer)
end

return display
