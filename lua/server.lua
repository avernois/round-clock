local M = {}

local tableInsert = table.insert
local createServer, net_TCP = net.createServer, net.TCP
local print, ipairs = print, ipairs
local stringFind, stringMatch = string.find, string.match
local setmetatable = setmetatable

if setfenv then
    setfenv(1, M) -- for 5.1
else
    _ENV = M -- for 5.2
end

local Request = {}
function Request:new(message)
    o = { }
    setmetatable(o, self)
    self.__index = self
    o.command = stringMatch(message, "(%u*) /.* HTTP.*\n")
    o.path = stringMatch(message, o.command .." (/.*) HTTP.*\n")
    o.body = stringMatch(message, '\n%s*\n(.*)')

    return o
end

local srv
local route = {}

local function start()
    srv:listen(80,function(conn)
       conn:on("receive", 
        function(conn,payload)
            local request = Request:new(payload)
            print("server: " .. request.command .. " " .. request.path)
            local matched = false
            for i, r in ipairs(route) do
                if (r.command == request.command and r.path == request.path) then
                    print("Match !", r.command.. " "..r.path)
                    local response = r.response(r.interpret(request))
                    conn:send(response)
                    matched = true
                    break
                end
            end

            if not matched then
                print("No route found.")
                conn:send('HTTP/1.1 404 NOT FOUND\n\n')
            end
            
       end)
       conn:on("sent", function(conn)
          conn:close()
          print("Connection closed")
       end)
    end)  

end

function M.addRoute(command, path, interpret, response)
    tableInsert(route, {command = command, path = path, interpret = interpret, response = response})
end

function M.start()
    srv = createServer(net_TCP)
    start()
end

function M.stop()
    srv:close()
end

return M
