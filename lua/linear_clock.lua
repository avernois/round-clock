local clock = {}

local tmrCreate, tmr_ALARM_AUTO = tmr.create, tmr.ALARM_AUTO

if setfenv then
    setfenv(1, clock) -- for 5.1
else
    _ENV = clock -- for 5.2
end

local timer = tmrCreate()
local display
local config
local time

function init(time_module, display_module, configuration)
    config = configuration
    display = display_module
    time = time_module
end

function clock.stop()
    timer:stop()
end

function clock.start()
    timer:alarm(50, tmr_ALARM_AUTO, function()
        now = time.get_zoned_time(2)
        display.display(now["hour"], now["min"], now["sec"], config)
    end)
end

return clock
